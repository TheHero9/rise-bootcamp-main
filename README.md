# 🍊 Rise Bootcamp Main



## ✍️ Author
Project by: Demetrios Vlassis


## 🚧Structure

### 📁 Folders: 
#### <pre>📁 [ProgrammingLanguage]</pre>
####    <pre>   📁 [TopicName] **(tasks for the current lecture)** </pre>
#####      <pre>      📁 [TopicName] Solution </pre>
######     <pre>         📁 [Taskname] </pre>
#####      <pre>      📁 [TopicName] Tests </pre>
######      <pre>         📁 [Taskname] Tests </pre>


### 🌳 Branches:
#### 🌿 [TopicName]

