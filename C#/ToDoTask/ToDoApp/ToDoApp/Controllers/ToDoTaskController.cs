﻿using Microsoft.AspNetCore.Mvc;
using ToDoApp.Models;
using ToDoApp.ToDoTaskServices;

namespace ToDoApp.Controllers
{
    public class ToDoTaskController : Controller
    {
        private  IToDoTaskServices _toDoServices { get; }

        public ToDoTaskController(IToDoTaskServices taskServices)
        {
            _toDoServices = taskServices;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var tasks = _toDoServices.GetAllTasks();

            return View(tasks);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateNewTask(ToDoTask task)
        {

            _toDoServices.CreateTask(task);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var task = _toDoServices.GetOneTask(id);
            return View(task);
        }

        [HttpPost]
        public IActionResult EditTask(ToDoTask task)
        {
            _toDoServices.EditTask(task);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var task = _toDoServices.GetOneTask(id);
            return View(task);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var task = _toDoServices.GetOneTask(id);

            _toDoServices.DeleteTask(task);
            return RedirectToAction("Index");
        }


    }
}
