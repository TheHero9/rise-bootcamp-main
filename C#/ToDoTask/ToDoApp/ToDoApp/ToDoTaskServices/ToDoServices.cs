﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using ToDoApp.Models;

namespace ToDoApp.ToDoTaskServices
{
    public class ToDoServices : IToDoTaskServices
    {
        private readonly ToDoTaskContext _db;

        public ToDoServices(ToDoTaskContext db)
        {
            _db = db;
        }

        [HttpGet]
        public List<ToDoTask> GetAllTasks()
        {
            var tasks = (from task in _db.ToDoTasks
                         join assignment in _db.Assignments
                         on task.AssignmentId equals assignment.Id
                         select new ToDoTask()
                         {
                             Id = task.Id,
                             Name = task.Name,
                             CreatedOn = task.CreatedOn,
                             Done = task.Done,
                             Description = task.Description,
                             AssignmentId = task.AssignmentId,
                             Assignment = assignment
                         }).ToList();

            return tasks;
        }

        [HttpGet]
        public ToDoTask GetOneTask(int id)
        {
            var task = _db.ToDoTasks
            .Include(t => t.Assignment)
            .FirstOrDefault(t => t.Id == id);

            return task;
        }

        [HttpPost]
        public void CreateTask(ToDoTask task)
        {
            Assignment newAssignment = new();
            newAssignment.CreatedBy = task.Assignment.CreatedBy;
            newAssignment.AssignedTo = task.Assignment.AssignedTo;

            ToDoTask newTask = new ToDoTask();
            newTask.Name = task.Name;
            newTask.Description = task.Description;
            newTask.Done = false;
            newTask.CreatedOn = task.CreatedOn;
            newTask.AssignmentId = newAssignment.Id;

            newTask.Assignment = newAssignment;

            _db.Add(newTask);
            _db.SaveChanges();
        }

        [HttpPost]
        public void EditTask(ToDoTask task)
        {
            var oldTask = _db.ToDoTasks
            .Include(t => t.Assignment)
            .FirstOrDefault(t => t.Id == task.Id);

            oldTask.Name = task.Name;
            oldTask.Description = task.Description;
            oldTask.Done = task.Done;
            oldTask.CreatedOn = task.CreatedOn;

            _db.SaveChanges();
        }

        [HttpGet]
        public void DeleteTask(ToDoTask task)
        {
            _db.Remove(task);
            _db.SaveChanges();
        }
    }
}
