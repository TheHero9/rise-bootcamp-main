﻿using Microsoft.Extensions.FileSystemGlobbing;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection;
using System.Runtime.Intrinsics.Arm;
using ToDoApp.Models;

namespace ToDoApp.ToDoTaskServices
{
    public interface IToDoTaskServices
    {
        List<ToDoTask> GetAllTasks();
        ToDoTask GetOneTask(int id);
        void CreateTask(ToDoTask task);
        void EditTask(ToDoTask task);
        void DeleteTask(ToDoTask task); 
    }
}
