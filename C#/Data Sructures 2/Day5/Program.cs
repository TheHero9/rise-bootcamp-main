﻿namespace UniqueElements
{
    public class Program
    {


        public static List<int> UniqueElements(List<int> input)
        {

            HashSet<int> visited = new HashSet<int>();


            for(int i=0; i< input.Count(); i++)
            {
                visited.Add(input[i]);
            }

            return visited.ToList();


        }

        static void Main(string[] args)
        {

            List<int> myNumbers = new List<int>{ 4, 4, 4, 4, 2, 29, 0 };

            List<int> test = UniqueElements(myNumbers);

            foreach (int number in test)
            {
                Console.WriteLine(number);
            }

            Console.WriteLine("Hello, World!");
        }
    }
}