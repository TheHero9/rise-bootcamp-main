using UniqueElements;

namespace UniqueElementsTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {

            List<int> input = new List<int> { 4,4,4,4 ,2, 29, 0};
            List<int> expected = new List<int> { 4, 2, 29, 0 };


            List<int> result = Program.UniqueElements(input);

            Assert.AreEqual(expected.Count(), result.Count());


            
            
        }


        [TestMethod]
        public void TestMethod2()
        {

            List<int> input = new List<int> { 1 };
            List<int> expected = new List<int> { 1 };


            List<int> result = Program.UniqueElements(input);

            Assert.AreEqual(expected.Count(), result.Count());




        }


        [TestMethod]
        public void TestMethod3()
        {

            List<int> input = new List<int> { 6,6,1,2,3,6,7,12,1 };
            List<int> expected = new List<int> { 6,1,2,3,7,12 };


            List<int> result = Program.UniqueElements(input);

            Assert.AreEqual(expected.Count(), result.Count());




        }
    }
}