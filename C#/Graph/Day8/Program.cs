﻿
using System;
using System.Collections.Generic;

namespace Day8
{
    
    public class Program
    {

        public static bool MakeGraph(int[][] graph, int nodes)
        {

            Dictionary<int, List<int>> ls = new Dictionary<int, List<int>>();

            bool[] visited = new bool[nodes];
            bool[] path = new bool[nodes];


            // Graph
            for (int i = 0; i < graph.Length; i++)
            {
                // Check if we have that node already
                if (!ls.ContainsKey(graph[i][0]))
                {
                    ls.Add(graph[i][0], new List<int>());
                }
                
                // Connect the nodes
                ls[graph[i][0]].Add(graph[i][1]);

            }
            // Start traversal
            for (int i = 0; i < nodes; i++)
            {
                // Check for cycle
                if (Dfs(ls, i, visited, path))

                    return true;

            }
            return false;

        }



        private static bool Dfs(Dictionary<int, List<int>> graph, int start, bool[] visited, bool[] path)
        {
            if (path[start])
            {
                return true;
            }

            visited[start] = true;

            path[start] = true;


            if (graph.ContainsKey(start))
            {
                // We start our traversal from our start node of the graph.
                foreach(var item in graph[start])
               
               {
                    // Recursion here
                    if (Dfs(graph, item, visited, path))
                    {
                        return true;
                    }

                }

            }
           
            path[start] = false;

            return false;

        }


        public static void Main(string[] args)
        {
           



           

        }
    }
}