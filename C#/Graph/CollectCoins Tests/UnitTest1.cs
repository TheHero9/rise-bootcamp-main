using Task2___CollectCoins;

namespace CollectCoins_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test1True()
        {

            int[][] input1 = new int[][]
               {
                    new int[] {3, 3},
                    new int[] {1, 2},
                    new int[] {2, 3},
                    new int[] {1, 3}
               };

            bool expected1 = true;
            bool results1 = Program.StartCollectingCoins(input1);

            Assert.AreEqual(expected1, results1);
            
        }

        [TestMethod]
        public void Test2False()
        {
            int[][] input2 = new int[][]
                {
                    new int[] {3, 2},
                    new int[] {1, 2},
                    new int[] {2, 1}
                };

            bool expected2 = false;
            bool results2 = Program.StartCollectingCoins(input2);
            Assert.AreEqual(expected2, results2);

        }

        [TestMethod]
        public void Test3True()
        {
            int[][] input3 = new int[][]
            {
                    new int[] {5, 6},
                    new int[] {1, 2},
                    new int[] {1, 3},
                    new int[] {2, 1},
                    new int[] {2, 3},
                    new int[] {2, 4},
                    new int[] {3, 1},
                    new int[] {3, 2},
                    new int[] {3, 5},
                    new int[] {4, 2},
                    new int[] {4, 5},
                    new int[] {5, 4},
                    new int[] {5, 3},
            };

            bool expected3 = true;
            bool results3 = Program.StartCollectingCoins(input3);
            Assert.AreEqual(expected3, results3);
        }
    }
}