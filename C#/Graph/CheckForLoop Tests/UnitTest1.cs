using Day8;

namespace CheckForLoop_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void NormalGraphFalse()
        {

            int[][] graph1 = new int[][]
                     {

                 new int[]{ 0,1},

                 new int[]{ 1,4},

                 new int[]{ 4,3},

                 new int[]{ 3,2},

                 new int[]{ 3,5},

                     };
            int nodes1 = 6;

            bool expected1 = false;
            var result1 = Program.MakeGraph(graph1, nodes1);

            Assert.AreEqual(expected1, result1);
        }


        [TestMethod]
        public void NormalGraphTrue()
        {

            int[][] graph2 = new int[][]
                     {

                 new int[]{ 0,1},

                 new int[]{ 1,2},

                 new int[]{ 2,3},

                 new int[]{ 2,0},

                 new int[]{ 3,4},

                     };
            int nodes2 = 5;

            bool expected2 = true;
            var result2 = Program.MakeGraph(graph2, nodes2);

            Assert.AreEqual(expected2, result2);
        }



        [TestMethod]
        public void Graph2ElementsFalse()
        {

            int[][] graph3 = new int[][]
                     {

                 new int[]{ 0,1},

                 new int[]{ 1,0},


                     };
            int nodes3 = 2;

            bool expected3 = true;
            var result3 = Program.MakeGraph(graph3, nodes3);

            Assert.AreEqual(expected3, result3);
        }

        public void Graph2ElementsTrue()
        {

            int[][] graph4 = new int[][]
                     {

                 new int[]{ 0,1},

                 new int[]{ 1,0},


                     };
            int nodes4 = 2;

            bool expected4 = true;
            var result4 = Program.MakeGraph(graph4, nodes4);

            Assert.AreEqual(expected4, result4);
        }
    }
}