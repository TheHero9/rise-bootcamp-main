CREATE DATABASE DIMI;

use DIMI

CREATE TABLE Product(
 maker char(1),
 model char(4),
 type char(7),

)

CREATE TABLE Printer(
 code int,
 model char(4),
 price decimal(10,2),

)

INSERT INTO Product (maker, model, type)
VALUES ('S', '23FF', 'ABCDEFG');

INSERT INTO Printer (code, model, price)
VALUES (1, 'RRTS', 19.89);


ALTER TABLE Printer
ADD type char(6)
CHECK (type in ('laser', 'matrix', 'jet'));

ALTER TABLE Printer
ADD color char(1) DEFAULT 'n'
CHECK (color in ('y', 'n'));

INSERT INTO Printer (code, model, price)
VALUES (1, 'RRTS', 20.89);

ALTER TABLE Printer
DROP COLUMN price;

DROP TABLE Product;
DROP TABLE Printer;