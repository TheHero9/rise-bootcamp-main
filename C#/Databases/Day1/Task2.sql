CREATE DATABASE FLIGHTS;

use FLIGHTS;

CREATE TABLE Airline(
code char(2) NULL,
name char(20) NOT NULL,
country char(30) NULL,
CONSTRAINT name_airline UNIQUE(name, country)
);

CREATE TABLE Airplane(
code char(3) NULL,
type char(40) NULL,
seats smallint NULL,
year smalldatetime NULL,
CONSTRAINT check_seats CHECK (seats>=0)
);

CREATE TABLE Flight(
fnumber char(6) NULL,
airline_operator char(2) NULL,
dep_airport char(3) NULL,
arr_airport char(3) NULL,
flight_time time NULL,
flight_duration int NULL,
airplane char(3) NULL
);

CREATE TABLE Airport(
code char(3) NOT NULL,
name char(50) NULL,
country char(30) NOT NULL,
city char(30) NOT NULL
);


CREATE TABLE Customer(
code int PRIMARY KEY NOT NULL,
fname char(40) NOT NULL,
lname char(40) NULL,
email varchar(255) NULL,
CONSTRAINT chk_email CHECK(email LIKE '%_@__%.__%')
);

CREATE TABLE Agency(
name char(45) NOT NULL,
country char(40) NOT NULL,
city char(45) NULL,
phone int NOT NULL
);

ALTER TABLE Agency
ADD phone_number int NULL;

ALTER TABLE Agency
ADD phone_number1 char(10) NULL;


CREATE TABLE Booking(
code char(6) PRIMARY KEY NOT NULL,
agency char(45) NOT NULL,
airplane_code char(2) NULL,
flight_number char(6) NOT NULL,
customer_id int NOT NULL,
boking_date smalldatetime NOT NULL,
flight_date smalldatetime NOT NULL,
price smallmoney NOT NULL,
status bit NULL,
);


INSERT INTO AIRLINE
VALUES
  ('AZ', 'Alitalia', 'Italy'),
  ('BA', 'British Airways', 'United Kingdom'),
  ('LH', 'Lufthansa', 'Germany'),
  ('SR', 'Swissair', 'Switzerland'),
  ('FB', 'Bulgaria Air', 'Bulgaria'),
  ('AF', 'Air France', 'France'),
  ('TK', 'Turkish Airlines', 'Turkey'),
  ('AA', 'American Airlines', 'United States');

  INSERT INTO AIRPORT
VALUES
  ('SOF', 'Sofia International', 'Bulgaria', 'Sofia'),
  ('CDG', 'Charles De Gaulle', 'France', 'Paris'),
  ('ORY', 'Orly', 'France', 'Paris'),
  ('LBG', 'Le Bourget', 'France', 'Paris'),
  ('JFK', 'John F Kennedy International', 'United States', 'New York'),
  ('ORD', 'Chicago O''Hare International','United States', 'Chicago'),
  ('FCO', 'Leonardo da Vinci International','Italy', 'Rome'),
  ('LHR', 'London Heathrow', 'United Kingdom', 'London');

  INSERT INTO Airplane
VALUES
  ('319', 'Airbus A319', 150, 1993),
  ('320', 'Airbus A320', 280, 1984),
  ('321', 'Airbus A321', 150, 1989),
  ('100', 'Fokker 100', 80, 1991),
  ('738', 'Boeing 737-800', 90, 1997),
  ('735', 'Boeing 737-800', 90, 1995);

  INSERT INTO FLIGHT
VALUES
  ('FB1363', 'AZ', 'SOF', 'ORY', '12:45', 100, '738'),
  ('FB1364', 'AZ', 'CDG', 'SOF', '10:00', 120, '321'),
  ('SU2060', 'AZ', 'LBG', 'SOF', '11:10', 110, '738'),
  ('SU2061', 'TK', 'SOF', 'JFK', '13:00', 110, '320'),
  ('FB363', 'FB', 'SOF', 'ORD', '15:10', 110, '738'),
  ('FB364', 'FB', 'LHR', 'SOF', '21:05', 120, '738');

  INSERT INTO CUSTOMER
VALUES
  (1, 'Petar', 'Milenov', 'petter@mail.com'),
  (2, 'Dimitar', 'Petrov', 'petrov@mail.com'),
  (3, 'Ivan', 'Ivanov', 'ivanov@mail.com'),
  (4, 'Petar', 'Slavov', 'slavov@mail.com'),
  (5, 'Bogdan', 'Bobov', 'bobov@mail.com');


  INSERT INTO AGENCY
VALUES
  ('Travel One', 'Bulgaria', 'Sofia', '0783482233'),
  ('Travel Two', 'Bulgaria', 'Plovdiv', '02882234'),
  ('Travel Tour', 'Bulgaria', 'Sofia', NULL),
  ('Aerotravel', 'Bulgaria', 'Varna', '02884233');


  INSERT INTO BOOKING
VALUES
  ('YN298P', 'Travel One', 'FB', 'FB1363', 1, '2013-11-18', '2013-12-25', 300, 0),
  ('YA298P', 'Travel Two', 'FB', 'FB1364', 2, '2013-12-18', '2013-12-25', 300, 1),
  ('YB298P', 'Travel Tour', 'FB', 'SU2060', 3, '2014-01-18', '2014-02-25', 400, 0),
  ('YC298P', 'Travel One', 'FB', 'SU2061', 4, '2014-11-11', '2014-11-25', 350, 0),
  ('YD298P', 'Travel Tour', 'FB', 'FB363', 1, '2013-11-03', '2013-12-20', 250, 1),
  ('YE298P', 'Aerotravel', 'FB', 'FB364', 2, '2013-11-07', '2013-12-21', 150, 0);