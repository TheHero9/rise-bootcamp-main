﻿namespace Root
{
    public class Program
    {

        // Newton-Raphson method for Cube Root
        public static int Root3(double num)
        {
            double cubeRoot = num / 3.0;
            double precision = 0.000001;
            double previous;

            do
            {
                previous = cubeRoot;
                cubeRoot = (2.0 * cubeRoot + num / (cubeRoot * cubeRoot)) / 3.0;
            }
            while (Math.Abs(previous - cubeRoot) > precision);

            if (num == 0) return 0;
            return (int)cubeRoot;
        }



        static void Main(string[] args)

        {
        }
    }
}