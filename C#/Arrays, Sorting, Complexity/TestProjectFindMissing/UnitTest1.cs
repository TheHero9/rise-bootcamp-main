using FindMissing;

namespace TestProjectFindMissing
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodNormal()
        {
            // Arrange
            int[] numbers = { 10, 8, 7, 5, 9 };

            int expected = 6;

            // Act
            int result = Program.FindMissingNumber(numbers);


            // Assert
            Assert.AreEqual(expected, result);

        }

        [TestMethod]
        public void TestmethodOneElement()
        {
            // Arrange
            int[] numbers = { 10 };

            int expected = 0;

            // Act
            int result = Program.FindMissingNumber(numbers);


            // Assert
            Assert.AreEqual(expected, result);

        }

        [TestMethod]
        public void TestMethod1()
        {
            // Arrange
            int[] numbers = { 10, 8, 7, 5, 9 };

            int expected = 6;

            // Act
            int result = Program.FindMissingNumber(numbers);


            // Assert
            Assert.AreEqual(expected, result);

        }
    }
}