
using Root;

namespace TestProjectRoot
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodNormal1()
        {
            // Arrange
            double number = 27;

            double expected = 3;

            // Act
            int result = Program.Root3(number);


            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMethodNormal2()
        {
            // Arrange
            double number = 729;

            double expected = 9;

            // Act
            int result = Program.Root3(number);


            // Assert
            Assert.AreEqual(expected, result);
        }


        [TestMethod]
        public void TestMethodWithZero()
        {
            // Arrange
            double number = 0;

            double expected = 0;

            // Act
            int result = Program.Root3(number);


            // Assert
            Assert.AreEqual(expected, result);
        }
    }
}